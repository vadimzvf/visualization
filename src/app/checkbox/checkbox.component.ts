import uniqueId from 'lodash/uniqueId';
import { Component, Output, Input, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-checkbox',
    templateUrl: './checkbox.component.html',
    styleUrls: ['./checkbox.component.css']
})
export class CheckboxComponent {
    private id: string;

    @Input()
    isChecked = false;
    @Input()
    text = ''; // is it possible to set React like children to component? O_o
    @Input()
    color = '#fff';
    @Output()
    toggle = new EventEmitter<boolean>();

    constructor() {
        this.id = uniqueId('checkbox_');
    }

    onToggle() {
        this.toggle.emit(!this.isChecked);
    }
}
