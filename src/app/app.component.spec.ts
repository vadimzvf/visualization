import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { RadarComponent } from './radar/radar.component';
import { ChannelsListComponent } from './channels-list/channels-list.component';
import { CheckboxComponent } from './checkbox/checkbox.component';

describe('AppComponent', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [AppComponent, RadarComponent, ChannelsListComponent, CheckboxComponent]
        }).compileComponents();
    }));

    it('should create the app', () => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    });
});
