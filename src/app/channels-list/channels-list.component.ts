import { Component, Output, Input, EventEmitter } from '@angular/core';

export interface IChannelData {
    name: string;
    color: string;
}

@Component({
    selector: 'app-channels-list',
    templateUrl: './channels-list.component.html',
    styleUrls: ['./channels-list.component.css']
})
export class ChannelsListComponent {
    @Input()
    channels: IChannelData[] = [];
    @Input()
    selectedChannels: string[] = [];

    @Output()
    toggle = new EventEmitter<string[]>();
    @Output()
    channelMouseEnter = new EventEmitter<string>();
    @Output()
    channelMouseLeave = new EventEmitter<string>();

    onToggle(channel: string) {
        const isSelected = this.selectedChannels.includes(channel);

        if (isSelected) {
            // to remove
            this.toggle.emit(this.selectedChannels.filter(c => c !== channel));
        } else {
            // to add
            this.toggle.emit([...this.selectedChannels, channel]);
        }
    }

    onChannelMouseEnter(channel: string) {
        this.channelMouseEnter.emit(channel);
    }

    onChannelMouseLeave(channel: string) {
        this.channelMouseLeave.emit(channel);
    }
}
