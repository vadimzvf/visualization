import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { RadarComponent } from './radar/radar.component';
import { ChannelsListComponent } from './channels-list/channels-list.component';
import { CheckboxComponent } from './checkbox/checkbox.component';

@NgModule({
    declarations: [AppComponent, RadarComponent, ChannelsListComponent, CheckboxComponent],
    imports: [BrowserModule],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {}
