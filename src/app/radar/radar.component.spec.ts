import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RadarComponent } from './radar.component';
import { ChannelsListComponent } from '../channels-list/channels-list.component';
import { CheckboxComponent } from '../checkbox/checkbox.component';

describe('RadarComponent', () => {
    let component: RadarComponent;
    let fixture: ComponentFixture<RadarComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [RadarComponent, ChannelsListComponent, CheckboxComponent]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(RadarComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
