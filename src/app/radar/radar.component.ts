import { lineRadial } from 'd3-shape';
import { scaleLinear } from 'd3-scale';
import { Component, HostListener, OnInit } from '@angular/core';
import DataService, { Columns } from '../services/data.service';
import { IChannelData } from '../channels-list/channels-list.component';

const MAX_RADIUS = 350;

// TODO: generate random colors?
const colors = ['#845EC2', '#D65DB1', '#FF6F91', '#FF9671', '#FFC75F', '#F9F871', '#00C9A7'];

interface IDataItem {
    name: string;
    data: number[];
}

interface IChannelsPath {
    name: string;
    path: string;
    color: string;
}

interface IDiagonalLine {
    name: string;
    nameTextAnchor: 'start' | 'end';
    nameTextAlignmentBaseline: 'hanging' | 'baseline';
    maxValue: number;
    x1: number;
    y1: number;
    x2: number;
    y2: number;
}

@Component({
    selector: 'app-radar',
    templateUrl: './radar.component.html',
    styleUrls: ['./radar.component.css']
})
export class RadarComponent implements OnInit {
    constructor(private dataService: DataService) {}
    private width = 1000;
    private height = 900;
    private scaleRadius = MAX_RADIUS;
    private channelsPaths: IChannelsPath[] = [];
    private channelsData: IChannelData[] = [];
    private selectedChannelsNames: string[] = [];
    private diagonals: IDiagonalLine[] = [];
    private mouse: { left: string; top: string } = { left: '0px', top: '0px' };
    private focusedChannelName = '';

    ngOnInit() {
        this.dataService.fetchData().subscribe(data => {
            this.setData(data);
        });
    }

    onMouseEnter(name: string) {
        this.focusedChannelName = name;
    }

    onMouseLeave(name: string) {
        this.focusedChannelName = '';
    }

    @HostListener('document:mousemove', ['$event'])
    mouseMove(event) {
        const TOOLTIP_MARGIN = 10;

        this.mouse.left = `${event.clientX + TOOLTIP_MARGIN}px`;
        this.mouse.top = `${event.clientY + TOOLTIP_MARGIN}px`;
    }

    onToggleChannel(nextNames: string[]) {
        this.selectedChannelsNames = nextNames;
    }

    getVisibleChannelsPaths(): IChannelsPath[] {
        return this.channelsPaths.filter(channel => this.selectedChannelsNames.includes(channel.name));
    }

    private setData(rawData) {
        const preparedChannelsData: IDataItem[] = rawData.data.map((columns: Columns) => {
            const [name, ...dataColumns] = columns;

            return { name, data: dataColumns };
        });

        const [nameColumnInfo, ...columnsInfo] = rawData.meta.columns;

        const maxColumnsValues = [];

        for (let i = 0; i < columnsInfo.length; i++) {
            const maxValue = Math.max(...preparedChannelsData.map(channelData => channelData.data[i]));
            const roundedMaxValue = Math.ceil(maxValue / 10) * 10;
            maxColumnsValues.push(roundedMaxValue);
        }

        const scales = maxColumnsValues.map(maxValue => {
            return scaleLinear()
                .domain([0, maxValue])
                .range([0, MAX_RADIUS]);
        });

        this.diagonals = columnsInfo.map((info, index) => {
            const angle = (Math.PI * 2 * index) / columnsInfo.length;
            const scaleRadius = MAX_RADIUS + 40;

            return {
                name: info.title,
                nameTextAnchor: angle <= Math.PI ? 'start' : 'end',
                nameTextAlignmentBaseline: (angle + Math.PI / 2) % (Math.PI * 2) <= Math.PI ? 'hanging' : 'baseline',
                maxValue: maxColumnsValues[index],
                x1: 0,
                y1: 0,
                x2: scaleRadius * Math.sin(angle),
                y2: scaleRadius * Math.cos(angle),
                scaleValueX: MAX_RADIUS * Math.sin(angle),
                scaleValueY: MAX_RADIUS * Math.cos(angle)
            };
        });

        this.channelsPaths = preparedChannelsData.map((item, index) => {
            const itemViewValues: [number, number][] = item.data.map((value, dataIndex) => {
                return [
                    (Math.PI * 2 * dataIndex) / item.data.length + Math.PI, // angle
                    scales[dataIndex](value) // radius
                ];
            });

            return {
                name: item.name,
                color: colors[index],
                path: lineRadial()(itemViewValues)
            };
        });

        this.channelsData = this.channelsPaths.map(channel => ({ name: channel.name, color: channel.color }));
        this.selectedChannelsNames = this.channelsData.map(channel => channel.name);
    }
}
