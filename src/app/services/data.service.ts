import { of } from 'rxjs';
import { delay } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import * as rawData from './data.json';

export type Columns = [
    string,
    number,
    number,
    number,
    number,
    number,
    number,
    number,
    number,
    number,
    number,
    number,
    number,
    number,
    number,
    number,
    number,
    number,
    number
];

@Injectable({ providedIn: 'root' })
export default class DataService {
    fetchData() {
        return of({ data: rawData.data as Columns[], meta: rawData.meta }).pipe(delay(500));
    }
}
