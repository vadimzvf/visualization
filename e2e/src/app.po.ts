import { browser, by, element } from 'protractor';

export class AppPage {
    navigateTo() {
        return browser.get(browser.baseUrl) as Promise<any>;
    }

    getFirstChannelTitle() {
        return element(by.css('app-root .channelsList label')).getText() as Promise<string>;
    }
}
